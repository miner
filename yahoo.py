#!/usr/bin/python

import urllib
import re

#
#These constants are specific to the Yahoo Finance 
#request URL
#

NAME              	= 'n'
ASK               	= 'a'
AVERAGE_DAILY_VOL 	= 'a2'
ASK_SIZE		= 'a5'
BID			= 'b'
ASK_RT			= 'b2'
BID_RT			= 'b3'
BOOK_VALUE		= 'b4'
BID_SIZE		= 'b6'
CHANGE_PERCENT_CHANGE	= 'c'
CHANGE			= 'c1'
COMMISSION		= 'c3'
CHANGE_RT		= 'c6'
AFTER_HOURS_CHANGE	= 'c8'
DIVIDEND_PER_SHARE	= 'd'
LAST_TRADE_DATE		= 'd1'
TRADE_DATE		= 'd2'
EPS			= 'e'
EPS_CUR_YEAR		= 'e7'
EPS_NEXT_YEAR		= 'e8'
EPS_NEXT_Q		= 'e9'
FLOAT_SHARES		= 'f6'
DAY_LOW			= 'g'
DAY_HIGH		= 'h'
LOW_52W			= 'j'
HIGH_52W		= 'k'
HOLDINGS_GAIN_PERCENT	= 'g1'
ANNUALIZED_GAIN		= 'g3'
HOLDINGS_GAIN 		= 'g4'
HOLDINGS_GAIN_PERCENT 	= 'g5'
HOLDINGS_GAIN_RT	= 'g6'
ORDER_BOOK		= 'i5'
MARKET_CAP		= 'j1'
MARKET_CAP_RT 		= 'j3'
EBITDA			= 'j4' 
CHANGE_FROM_52W_LOW	= 'j5'
PER_CHANGE_FROM_52W_LOW = 'j6'
LAST_TRADE_WITH_TIME_RT = 'k1'
CHANGE_PERCENT_RT 	= 'k2'
LAST_TRADE_SIZE		= 'k3'
CHANGE_FROM_52W_HIGH	= 'k4'
PER_CHANGE_FROM_52W_HIGH= 'k5'
LAST_TRADE_WITH_TIME	= 'l'
LAST_TRADE_PRICE	= 'l1'
HIGH_LIMIT 		= 'l2'
LOW_LIMIT		= 'l3'
DAYS_RANGE		= 'm'
DAYS_RANGE_RT		= 'm2'
MA_50D			= 'm3'
MA_200D			= 'm4'
CHANGE_FROM_200D_MA	= 'm5'
PERC_CHANGE_FROM_200D_MA= 'm6'
CHANGE_FROM_50D_MA	= 'm7'
PERC_CHANGE_FROM_50D_MA = 'm8'
NOTES			= 'n4'
OPEN			= 'o'
PREVIOUS_CLOSE		= 'p'
PRICE_PAID		= 'p1'
PERCENT_CHANGE		= 'p2'
PRICE_PER_SALES		= 'p5'
PRICE_PER_BOOK		= 'p6'
EX_DIVIDEND_DATE	= 'q'
PE			= 'r'
DIVIDEND_PAY_DATE	= 'r1'
PE_RT			= 'r2'
PEG			= 'r5'
PRICE_PER_EPS_CUR_YEAR	= 'r6'
PRICE_PER_EPS_NEXT_YEAR = 'r7'
SYMBOL			= 's'
SHARES_OWNED		= 's1'
SHORT_RATIO		= 's7'
LAST_TRADE_TIME		= 't1'
TRADE_LINKS		= 't6'
TICKER_TREND		= 't7'
ONE_YEAR_TARGET		= 't8'
VOLUME			= 'v'
HOLDINGS_VALUE		= 'v1'
HOLDINGS_VALUE_RT	= 'v7'
RANGE_52W		= 'w'
DAY_VALUE_CHANGE	= 'w1'
DAY_VALUE_CHANGE_RT	= 'w4'
STOCK_EXCHANGE		= 'x'


valid_requests = ['n' , 'a', 'a2', 'a5', 'b', 'b2', 'b3', 'b4', 'b6', 'c', 'c1', 'c3', 'c6', 'c8', 'd', 'd1', 'd2', 'e', 'e7', 'e9', 'f6', 'g', 'h', 'j', 'k', 'g1', 'g3', 'g4', 'g5', 'g6', 'i5', 'j1', 'j3', 'j4', 'j5', 'j6', 'k1', 'k2', 'k3', 'k4', 'k5', 'l', 'l1', 'l2', 'l3', 'm', 'm2', 'm3', 'm4', 'm5', 'm6', 'm7', 'm8', 'n4', 'o', 'p', 'p1', 'p2', 'p5', 'p6', 'q', 'r', 'r1', 'r2', 'r5', 'r6', 'r7', 's', 's1', 's7', 't1', 't6', 't7', 't8', 'v', 'v1', 'v7', 'w', 'w1', 'w4', 'x']

def check_request(field):
	p = re.compile('[a-z][0-9]|[a-z]')
	list =  p.findall(field)
        for user_entry in list:
            valid = 0
            for word in valid_requests:
                if word == user_entry:
                     valid = 1
            if valid == 0:
                print "Error, malfomed request: " + user_entry
                print "Please ensure that the symbols you are requesting are part of the Yahoo API"	
                print "fatal: Exiting program"
                quit()

def yahoo_request(sym, field):

	check_request(field)

	sym = sym.upper()

	prefix = "http://finance.yahoo.com/d/quotes.csv?s="
	url = prefix + sym+ "&f=" + field	
	print "Requesting: " + url
	data = urllib.urlopen(url).read()

	data_list = list()
	data_list = data.split(',')

	return data_list



def yahoo_hist_request(sym, start, end):
	prefix = "http://ichart.yahoo.com/table.csv?s=" 
	suffix = "&ignore=.csv"

	start_list = start.split('-')
	end_list = end.split('-')

	if (len(start_list) != len(end_list) != 3):
		print "Error, stockr yahoo historical api does not understand "\
			+ start + " or " + end + ". Use the format 2008-1-30" 

	start_date_url = "&g=d&a=" + start_list[2] + "&b=" + start_list[1] + "&c=" + start_list[0]
	end_date_url   = "&d=" + end_list[2] + "&e=" + end_list[1] + "&f=" + end_list[0]

	url = prefix + sym + end_date_url + start_date_url + suffix

	print "Request url for historical quote: " + url
	data = urllib.urlopen(url).read()

	print data




#yahoo_hist_request("XOM", "2007-1-1", "2008-1-1")

print yahoo_request("XOm", NAME + AVERAGE_DAILY_VOL)

